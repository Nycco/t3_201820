package test;


import model.data_structures.Stack;

import org.junit.Test;
import static org.junit.Assert.*;

public class StackTest {
	
	private Stack<String> stack ;
	
	
	public void setup1(){
		stack = new Stack<>();
		stack.push("a");
		stack.push("b");
		stack.push("c");
		stack.push("d");
		stack.push("e");
	}
	
	@Test
	public void testPop(){
		setup1();
		String str = stack.pop();
		assertEquals("e", str);
		str = stack.pop();
		assertEquals("d", str);
	}
	
	@Test
	public void testPush(){
		setup1();
		stack.push("f");
		assertEquals("f", stack.pop());
	}
	
	@Test
	public void testGetSize(){
		setup1();
		assertEquals(5, stack.size());
		stack.pop();
		assertEquals(4, stack.size());
	}
	
	@Test 
	public void testIsEmpty(){
		setup1();
		assertFalse("Deberia ser falso", stack.isEmpty());
		for(String str : stack){
			stack.pop();
		}
		assertTrue("Deberia ser verdadero", stack.isEmpty());
	}
}
