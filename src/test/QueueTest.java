package test;

import org.junit.Test;

import model.data_structures.Queue;

import static org.junit.Assert.*;


public class QueueTest {

	private Queue<String> list;
	
	public void setup1(){
	list = new Queue<>();
	list.enqueue("a");
	list.enqueue("b");
	list.enqueue("c");
	list.enqueue("d");
	list.enqueue("e");
	
	}
	
	public void setup2(){
		list = new Queue<>();
		list.enqueue("n");
		list.enqueue("o");
		list.enqueue("mo");
		list.enqueue("po");
	}

	@Test
	public void testDequeue(){
		setup1();
		String str = list.dequeue();
		assertEquals("a", str);
		str = list.dequeue();
		assertEquals("b", str);
	}
	
	@Test
	public void testGetSize(){
		setup1();
		assertEquals(5, list.size());
		list.dequeue();
		assertEquals(4, list.size());
	}
	
	@Test 
	public void testIsEmpty(){
		setup1();
		assertFalse("Deberia ser falso", list.isEmpty());
		for(String str : list){
			list.dequeue();
		}
		assertTrue("Deberia ser verdadero", list.isEmpty());
	}
	
}

