package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<T> implements IQueue<T> {
	private Node<T> head,tail;
	private int counter;

	public Queue(){
		head = null;
		tail = null;
		counter = 0;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() {

			private Node<T> currentNode = head;

			@Override
			public boolean hasNext() {
				return currentNode != null;
			}

			@Override
			public T next() {
				if (currentNode == null) {
					throw new NoSuchElementException();
				}
				T toReturn = currentNode.data;
				currentNode = currentNode.next;
				return toReturn;
			}
		};
	}


	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return counter == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return counter;
	}

	@Override
	public void enqueue(T t) {
		// TODO Auto-generated method stub
		Node<T> previousTail = tail;
		tail = new Node<T>(t);
		if(isEmpty()){
			head = tail;
		}
		else{
			previousTail.next = tail;
		}
		counter++;
	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		if(isEmpty()){
			throw new NoSuchElementException("Queue underflow");
		}
		T trash;
		trash = head.data;
		head = head.next;
		counter--;
		if(isEmpty()) tail = null;
		return trash;
	}

	static class Node<U> {
		U data;
		private Node<U> next;
		public Node(U element){
			next = null;
			data = element;
		}
	}

}
