package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class Stack<T> implements IStack<T>{

	private Node<T> first;
	private int nodecount;
	
	
	public Stack() {
		first = null;
		nodecount = 0;
	}
	
	

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() {

			private Node<T> followingNode = first;

	        @Override
	        public boolean hasNext() {
	            return followingNode != null;
	        }

	        @Override
	        public T next() {
	            if (followingNode == null) {
	                throw new NoSuchElementException();
	            }
	            T toReturn = followingNode.data;
	            followingNode = followingNode.next;
	            return toReturn;
	        }
		};
	}




	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return nodecount == 0;
	}




	@Override
	public int size() {
		// TODO Auto-generated method stub
		return nodecount;
	}




	@Override
	public void push(T t) {
		// TODO Auto-generated method stub
		Node<T> newNode = new Node<T>(t);
		if(first == null){
			first  = newNode;
		}
		else{
			newNode.next = first;
			first = newNode;
		}
		nodecount++;
	}




	@Override
	public T pop() {
		// TODO Auto-generated method stub
		if(first == null)
		{
			return null;
		}
		Node<T> popped = first;
		first = first.next;
		nodecount--;
		return popped.data;
	}
	
	
	static class Node<U>{
		
		U data;
		Node<U> next;
		public Node(U element){
			next = null;
			data = element;
		}
	}
}
