package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T> {

	private Node<T> firstNode;
	
	private Node<T> lastNode;
	
	private Node<T> curreNode;
	
	int nodeCount;
	
	public DoublyLinkedList() {
		firstNode = null;
		lastNode = null;
		curreNode = null;
		nodeCount =  0;
	}

	@Override
	public Iterator<T> iterator() {
		
		return new Iterator<T>() {

			private Node<T> followingNode = firstNode;

	        @Override
	        public boolean hasNext() {
	            return followingNode != null;
	        }

	        @Override
	        public T next() {
	            if (followingNode == null) {
	                throw new NoSuchElementException();
	            }
	            T toReturn = followingNode.data;
	            followingNode = followingNode.nextNode;
	            return toReturn;
	        }
		};
		
	}

	@Override
	public void add(T element) {
		Node<T> newNode = new Node<T>(element);
		if(firstNode == null){
			lastNode = newNode;
		}
		else{
			newNode.nextNode = firstNode;
			firstNode.previousNode = newNode;
		}
		firstNode = newNode;
		nodeCount++;
	}

	@Override
	public void addAtEnd(T element) {
		Node<T> currentNode = new Node<T>(element);
		if(firstNode == null){
			firstNode = currentNode;
		}
		else{
			lastNode.nextNode = currentNode;
			currentNode.previousNode = lastNode;
		}
		lastNode = currentNode;
		nodeCount++;
	}

	@Override
	public boolean addAtK(T element, int index) {
		 Node<T> currentNode;

	        if (getNode(index) != null){
	            Node<T> newNode = new Node<T>(element);
	            currentNode = getNode(index);

	            if (currentNode.previousNode != null) {
	                currentNode.previousNode.nextNode = newNode;
	                newNode.previousNode = currentNode.previousNode;
	                currentNode.previousNode = newNode;
	                newNode.nextNode = currentNode;
	            } else {
	                currentNode.previousNode = newNode;
	                newNode.nextNode = currentNode;
	            }
	            currentNode = newNode;

	          

	            return true;
	        } else {
	            return false; 
	        }		
	}
	
	@Override
	public boolean delete(T element) {
		// TODO Auto-generated method stub
		boolean deleted = false;
		if(firstNode != null && firstNode.data == element){
			firstNode = firstNode.nextNode;
			if(firstNode != null) firstNode.previousNode = null;
			deleted = true;
		}
		else{
			Node<T> currentNode = curreNode == null ? firstNode : curreNode;
			while(currentNode.nextNode != null && !deleted){
				if(currentNode.data == element){
					if(currentNode.previousNode != null){
						currentNode.previousNode.nextNode = currentNode.nextNode;
					}
					if(currentNode.nextNode != null){
						currentNode.nextNode.previousNode = currentNode.previousNode;
					}
					deleted = true;
				}
				currentNode = currentNode.nextNode;
			}
			if(lastNode != null && lastNode.data == element){
				lastNode = lastNode.previousNode;
				if(lastNode != null) lastNode.nextNode = null;
				deleted = true;
			}

		}
		return deleted;
	}

	@Override
	public boolean deleteAtK(int pos) {
		// TODO Auto-generated method stub
		Node<T> currentNode;
	
	    if (getNode(pos) != null){
	        currentNode = this.getNode(pos);
	
	        if(currentNode.previousNode != null) {
	            currentNode.nextNode.previousNode = currentNode.previousNode;
	            currentNode.previousNode.nextNode = currentNode.nextNode;
	        } else if (currentNode.nextNode != null){
	            currentNode.nextNode.previousNode = null;
	        } else if (isEmpty()){
	            lastNode = null;
	        }
	
	       
	        nodeCount--;
	        return true;
	    }else {
	        return false;
	    }
	}
	
	@Override
	public T getElement(int pos){
		return (getNode(pos) == null) ? null : getNode(pos).data;
	}
	
	@Override
	public T getCurrentElement() {
		// TODO Auto-generated method stub
		return (getCurrentNode() == null) ? null: getCurrentNode().data;
	}
	
	
	private Node<T> getNode(int pos) {
		// TODO Auto-generated method stub
		if(isEmpty() || pos < 0 || pos >= nodeCount){
            return null;
        }
        Node<T> currentNode = firstNode;

        for(int i = 0; i < pos; i++){
        	currentNode = currentNode.nextNode;
        }

        return currentNode;
	}
	
	
	


	@Override
	public int getSize() {
		return nodeCount;
	}

	private Node<T> getCurrentNode() {
		return curreNode;
	}

	public boolean isEmpty(){
		return nodeCount == 0;
	}
	
	static class Node<U>{
		U data;
		Node<U> nextNode;
		Node<U> previousNode;
		
		public Node(U data) {
			nextNode = null;
			previousNode = null;
			this.data = data;
		}
		
		public Node<U> getNext(){
			return nextNode;
		}
		
		public Node<U> getBefore(){
			return previousNode;
		}
		
		public U getData(){
			return data;
		}
	}

	

}
