package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {
	private int tripId; 
	
	private String startTime;
	
	private String endTime;
	
	private int bikeId;
	private int tripDuration;
	private int fromStationId;
	private String fromStationName;
	private int toStationId;
	private String toStationName;
	private String usertype;
	private String gender;
	private int birthyear;
	
	
	public VOTrip(int tripId, String startTime, String endTime, int bikeId, int tripDuration, int fromStationId, String fromStationName, int toStationId, String toStationName, String usertype, String gender, int birthyear){
		this.tripId = tripId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.bikeId = bikeId;
		this.tripDuration = tripDuration;
		this.fromStationId = fromStationId;
		this.fromStationName = fromStationName;
		this.toStationId = toStationId;
		this.toStationName = toStationName;
		this.usertype = usertype;
		this.gender = gender;
		this.birthyear = birthyear;
	}
	
	public VOTrip(int tripId, String startTime, String endTime, int bikeId, int tripDuration, int fromStationId, String fromStationName, int toStationId, String toStationName, String usertype){
		this(tripId, startTime, endTime, bikeId, tripDuration, fromStationId, fromStationName, toStationId, toStationName, usertype, null, 0);
	}
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return tripId;
	}	
	
	public int getToStationId() {
		return toStationId;
	}
	
	public int getBikeId(){
		return bikeId;
	}
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return tripDuration;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return fromStationName;
	}
	
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return toStationName;
	}
	
	public String getGender() {
		return gender;
	}
	
	public String toString() {
		return tripId + usertype;
	}
}
