package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager implements IDivvyTripsManager {
	private Stack<VOStation> stackStations = new Stack<>();
	
	private Stack<VOTrip> stackTrips = new Stack<>();
	private Queue<VOTrip> queueTrips = new Queue<>();
	
	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub
		try {
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
			String [] line = reader.readNext();
			while((line = reader.readNext()) != null){
				int id = Integer.parseInt(line[0]);
				String name = line[1];
				String city = line[2];
				double latitude = Double.parseDouble(line[3]);
				double longitude = Double.parseDouble(line[4]);
				int capacity = Integer.parseInt(line[5]);
				String date = line[6];
				VOStation station = new VOStation(id, name, city, latitude, longitude, capacity, date);
				stackStations.push(station);
			}
		    reader.close();

			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		try {
			
			CSVReader reader = new CSVReader(new FileReader(tripsFile));
			String [] line = reader.readNext();
			while((line = reader.readNext()) != null){
				String user = line[9];
				int birthyear = (line[11].equals(""))? 0 : Integer.parseInt(line[11]);
				if(user.toLowerCase().equals("subscriber")){
					
					
					VOTrip trip = new VOTrip(Integer.parseInt(line[0]), line[1], line[2], Integer.parseInt(line[3]), Integer.parseInt(line[4]),
										Integer.parseInt(line[5]), 
										line[6], Integer.parseInt(line[7]), 
										line[8], line[9], line[10], 
										birthyear);
					stackTrips.push(trip);
					queueTrips.enqueue(trip);
				}
				else if(user.toLowerCase().equals("customer")){
					
					VOTrip trip = new VOTrip(Integer.parseInt(line[0]), line[1], line[2], Integer.parseInt(line[3]), Integer.parseInt(line[4]),
										Integer.parseInt(line[5]), line[6], Integer.parseInt(line[7]), line[8], line[9]);
					stackTrips.push(trip);
					queueTrips.enqueue(trip);
				}
			
				
			}
			reader.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		// TODO Auto-generated method stubs
		DoublyLinkedList<String> list = new DoublyLinkedList<>();
		Iterator<VOTrip> iter = queueTrips.iterator();
		while(iter.hasNext() && n > 0){
			VOTrip trip = iter.next();
			int bikeId = trip.getBikeId();
			if(bikeId == bicycleId){
				list.addAtEnd("origen: " + trip.getFromStation() + "| destino: " + trip.getToStation());
				n--;
			}
		}
		return list;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {
		// TODO Auto-generated method stub
		if(n == 0) return null;
		Iterator<VOTrip> iter = stackTrips.iterator();
		VOTrip trip = null;
		int cont = 0;
		while(iter.hasNext() ){
			trip = iter.next();
			if(trip.getToStationId() == stationID){
				cont++;
				if(cont == n) break;
			}
			
		}
		return trip;
	}	


}
